APP_NAME = idiff

VERSION:=$(shell cat VERSION)
ifeq ($(CI_COMMIT_SHORT_SHA),)
	CI_COMMIT_SHORT_SHA:=$(shell git rev-parse --short HEAD)
endif
COMPILE_DATE:=$(shell LC_TIME=C date +"%F %T %z")
SHORT_DATE:=$(shell LC_TIME=C date +"%Y%m%d")

DEB_VERSION = $(VERSION)-$(SHORT_DATE)
DEB_PATH = debian/$(APP_NAME)
DEB_PKG = debian/$(APP_NAME).deb
DEB_PKG_FINAL = debian/$(APP_NAME)_$(DEB_VERSION)_all.deb


ui: idiff/ui_about_dialog.py idiff/ui_main_window.py

idiff/ui_about_dialog.py: idiff/ui_about_dialog.ui
	m4  -D M4_APP_VERSION="$(VERSION)" \
		-D M4_GIT_REVISION="$(CI_COMMIT_SHORT_SHA)" \
		-D M4_COMPILE_DATE="$(COMPILE_DATE)" \
		-P idiff/ui_about_dialog.ui >idiff/ui_about_dialog.tmp.ui
	pyuic5 -o idiff/ui_about_dialog.py idiff/ui_about_dialog.tmp.ui

idiff/ui_main_window.py: idiff/ui_main_window.ui
	pyuic5 -o idiff/ui_main_window.py idiff/ui_main_window.ui

run: ui
	python3 -m idiff

test: test-mypy

test-mypy: ui
	mypy --ignore-missing-imports idiff/*.py

pip-install:
	sed <setup.py.in "s/{{VERSION}}/$(VERSION)/" >setup.py
	pip3 install .

pip-uninstall:
	pip3 uninstall idiff

deb: clean deb-clean ui
	install -d $(DEB_PATH)/usr/bin
	install -m 755 bin/idiff $(DEB_PATH)/usr/bin/idiff

	install -d $(DEB_PATH)/usr/lib/python3/dist-packages/idiff
	cp -r idiff/*.py $(DEB_PATH)/usr/lib/python3/dist-packages/idiff/

# 	mkdir -p $(DEB_PATH)/usr/share/icons/hicolor/16x16/apps
# 	cp images/icon-16.png $(DEB_PATH)/usr/share/icons/hicolor/16x16/apps/idiff.png

	mkdir -p $(DEB_PATH)/usr/share/applications
	cp se.entropic.idiff.desktop $(DEB_PATH)/usr/share/applications/

	mkdir -p $(DEB_PATH)/usr/share/doc/$(APP_NAME)
	cp LICENSE $(DEB_PATH)/usr/share/doc/$(APP_NAME)/copyright
	cp README.md $(DEB_PATH)/usr/share/doc/$(APP_NAME)/

	install -d $(DEB_PATH)/DEBIAN
	sed <debian/control.tpl "s/{VERSION}/$(DEB_VERSION)/" >$(DEB_PATH)/DEBIAN/control
	dpkg-deb --root-owner-group --build $(DEB_PATH)
	mv $(DEB_PKG) $(DEB_PKG_FINAL)

deb-repo: deb
	update-deb-repo.sh $(DEB_PKG_FINAL)

clean:
	rm -f idiff/ui_about_dialog.py
	rm -f idiff/ui_main_window.py
	rm -f idiff/ui_about_dialog.tmp.ui
	rm -rf idiff/__pycache__
	rm -rf __pycache__
	rm -rf .mypy_cache

deb-clean:
	rm -rf $(DEB_PATH)
	rm -rf debian/*.deb
