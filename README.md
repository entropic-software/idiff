# idiff

Utility to interactively view differences of two texts

## Screenshots

![Side by side, colorized](screenshot1.png)

![Side by side, monochrome](screenshot2.png)

![Unified diff format](screenshot3.png)


## Installation

Easiest way to install on Debian is from my repo: https://entropic.se/debian

In this way updates can be installed with the standard Debian method.

You can also install with pip. Run `make pip-install` to install as user,
or `sudo make pip-install` to install system-wide.

## Requirements

- Python 3.7
- Qt 5.11
- PyQt5

(see debian/control.tpl for detailed dependency list)

### Requirements for building

`apt install pyqt5-dev-tools m4 make`

## Author

Created by Tomas Åkesson <tomas@entropic.se>

See LICENSE for license details.

Project homepage: https://gitlab.com/entropic-software/idiff
