Package: idiff
Version: {VERSION}
Section: text
Priority: optional
Architecture: all
Depends: python3, python3-pyqt5
Maintainer: Tomas Åkesson <tomas@entropic.se>
Homepage: https://gitlab.com/entropic-software/idiff
Description: Interactive diff utility
 Utility to interactively view differences of two texts
