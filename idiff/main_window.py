from datetime import datetime
import difflib
import html
import os
import re
from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtGui import QFont, QClipboard, QDropEvent
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from typing import Dict, List, Optional

from idiff.ui_main_window import Ui_MainWindow
from idiff.about_dialog import AboutDialog

SCROLL_UP: int = 1
SCROLL_DOWN: int = 2
SCROLL_LEFT: int = 3
SCROLL_RIGHT: int = 4
SCROLL_PAGE_UP: int = 5
SCROLL_PAGE_DOWN: int = 6
SCROLL_HOME: int = 7
SCROLL_END: int = 8


class MainWindow(QMainWindow, Ui_MainWindow):
    def __init__(self, parent = None):
        QMainWindow.__init__(self, parent=parent)
        self.setupUi(self)

        self.font_mono: QFont = QFont('Monospace')
        self.text_1.setFont(self.font_mono)
        self.text_2.setFont(self.font_mono)
        self.text_diff.setFont(self.font_mono)

        self.clipboard: QClipboard = QApplication.clipboard()

        self.text_1.textChanged.connect(self.text_changed)
        self.text_2.textChanged.connect(self.text_changed)

        self.text_1.dropEvent = lambda ev : self.load_dropped_file_1(ev)
        self.text_2.dropEvent = lambda ev : self.load_dropped_file_2(ev)

        self.action_clipboard_1.triggered.connect(self.copy_from_clipboard1)
        self.action_clipboard_2.triggered.connect(self.copy_from_clipboard2)
        self.action_diff_side_by_side.triggered.connect(self.toggle_display_mode)
        self.action_colorized_diff.triggered.connect(self.toggle_color_mode)

        self.action_diff_side_by_side.setChecked(True)

        self.action_about.triggered.connect(self.about)
        self.action_quit.triggered.connect(self.close)

        self.diff_styles = {
            "add_color": "#aaffaa",
            "chg_color": "#aaaaff",
            "sub_color": "#ffaaaa",
        }
        self.restore_config()
        self.html_diff: HtmlDiff = difflib.HtmlDiff()
        self.update_styles()

        # Use a timer to prevent updating the diff on every textChanged event
        self.timer: QTimer = QTimer(self)
        self.timer.setSingleShot(True)
        self.timer.timeout.connect(self.on_timer_update_diff)


    # Modify html diff styles, based on config if set
    def update_styles(self):
        # print(self.html_diff._styles)
        # print(self.html_diff._file_template)
        # print(self.html_diff._table_template)

        if self.colorized_diff():
            self.html_diff._styles: str = """
                table.diff {font-family:%s;border:medium;}
                table.diff td {padding: 0 1em;}
                .diff_header {background-color:#e0e0e0}
                td.diff_header {text-align:right}
                .diff_next {background-color:#c0c0c0}
                .diff_add {background-color:%s}
                .diff_chg {background-color:%s}
                .diff_sub {background-color:%s}
            """ % (
                self.font_mono.family(),
                self.diff_styles["add_color"],
                self.diff_styles["chg_color"],
                self.diff_styles["sub_color"],
            )
        else:
            self.html_diff._styles: str = """
                table.diff {font-family:%s;border:medium;}
                table.diff td {padding: 0 1em;}
                .diff_header {background-color:#e0e0e0}
                td.diff_header {text-align:right}
                .diff_next {background-color:#c0c0c0}
                .diff_add {font-weight: bold;}
                .diff_chg {text-decoration: underline;}
                .diff_sub {color: #cccccc;}
            """ % (
                self.font_mono.family()
            )


    def load_file(self, filename: str, text_edit_num: int):
        if text_edit_num == 1:
            self.load_file_in_textarea(filename, self.text_1)
        elif text_edit_num == 2:
            self.load_file_in_textarea(filename, self.text_2)
        else:
            raise(Exception("Invalid text_edit id"))


    def load_dropped_file_1(self, event: QDropEvent):
        self.load_dropped_file(event, self.text_1)

    def load_dropped_file_2(self, event: QDropEvent):
        self.load_dropped_file(event, self.text_2)


    def load_dropped_file(self, event: QDropEvent, text_edit: QTextEdit):
        if event.mimeData().hasUrls():
            for url in event.mimeData().urls():
                filename: str = url.toLocalFile()
                if filename != "":
                    self.load_file_in_textarea(filename, text_edit)
                    return # only load the first file


    def load_file_in_textarea(self, filename: str, text_edit: QTextEdit):
        if not os.path.isfile(filename) or not os.access(filename, os.R_OK):
            return

        with open(filename, "r") as f:
            lines: List[str] = f.readlines()
            content: str = ""
            for line in lines:
                content += line
            text_edit.setPlainText(content)


    def keyPressEvent(self, event):
        if event.modifiers() == QtCore.Qt.ControlModifier:
            if event.key() == QtCore.Qt.Key_Q:
                self.close()
            elif event.key() == QtCore.Qt.Key_F1:
                self.copy_from_clipboard1()
            elif event.key() == QtCore.Qt.Key_F2:
                self.copy_from_clipboard2()

        elif event.modifiers() == QtCore.Qt.AltModifier:
            if event.key() == QtCore.Qt.Key_Up:
                self.scroll_diff(SCROLL_UP)
            elif event.key() == QtCore.Qt.Key_Down:
                self.scroll_diff(SCROLL_DOWN)
            elif event.key() == QtCore.Qt.Key_Left:
                self.scroll_diff(SCROLL_LEFT)
            elif event.key() == QtCore.Qt.Key_Right:
                self.scroll_diff(SCROLL_RIGHT)

            elif event.key() == QtCore.Qt.Key_PageUp:
                self.scroll_diff(SCROLL_PAGE_UP)
            elif event.key() == QtCore.Qt.Key_PageDown:
                self.scroll_diff(SCROLL_PAGE_DOWN)

            elif event.key() == QtCore.Qt.Key_Home:
                self.scroll_diff(SCROLL_HOME)
            elif event.key() == QtCore.Qt.Key_End:
                self.scroll_diff(SCROLL_END)

        elif event.key() == QtCore.Qt.Key_Escape:
            self.close()


    def about(self):
        AboutDialog(self).exec()


    def scroll_diff(self, direction: int):
        vertical = [
            SCROLL_UP,
            SCROLL_DOWN,
            SCROLL_PAGE_UP,
            SCROLL_PAGE_DOWN,
            SCROLL_HOME,
            SCROLL_END,
        ]

        if direction in vertical:
            scrollbar = self.text_diff.verticalScrollBar()
        else:
            scrollbar = self.text_diff.horizontalScrollBar()

        if direction == SCROLL_UP or direction == SCROLL_LEFT:
            scrollbar.triggerAction(QAbstractSlider.SliderSingleStepSub)

        elif direction == SCROLL_DOWN or direction == SCROLL_RIGHT:
            scrollbar.triggerAction(QAbstractSlider.SliderSingleStepAdd)

        elif direction == SCROLL_PAGE_UP:
            scrollbar.triggerAction(QAbstractSlider.SliderPageStepSub)

        elif direction == SCROLL_PAGE_DOWN:
            scrollbar.triggerAction(QAbstractSlider.SliderPageStepAdd)

        elif direction == SCROLL_HOME:
            scrollbar.triggerAction(QAbstractSlider.SliderToMinimum)

        elif direction == SCROLL_END:
            scrollbar.triggerAction(QAbstractSlider.SliderToMaximum)


    def closeEvent(self, event):
        self.save_config()


    def save_config(self):
        settings = QSettings()
        settings.beginGroup("MainWindow")
        settings.setValue("Size", self.size())
        settings.setValue("Pos", self.pos())
        settings.setValue("Splitter", self.splitter.saveState())
        settings.setValue("ColorizedDiff", self.colorized_diff())
        settings.setValue("DisplaySideBySide", self.side_by_side())
        settings.endGroup()


    def restore_config(self):
        settings = QSettings()
        settings.beginGroup("MainWindow")

        pos = settings.value("Pos")
        if pos: self.move(pos)

        size = settings.value("Size")
        if size: self.resize(size)

        splitterSize = settings.value("Splitter")
        if splitterSize: self.splitter.restoreState(splitterSize)

        colorized_diff = settings.value("ColorizedDiff")
        if colorized_diff:
            self.action_colorized_diff.setChecked(
                True if colorized_diff == "true" else False
            )

        side_by_side = settings.value("DisplaySideBySide")
        if side_by_side:
            self.action_diff_side_by_side.setChecked(
                True if side_by_side == "true" else False
            )

        settings.endGroup()


        settings.beginGroup("Styles")

        add_color = settings.value("AddedColor")
        if add_color: self.diff_styles["add_color"] = add_color

        sub_color = settings.value("RemovedColor")
        if sub_color: self.diff_styles["sub_color"] = sub_color

        chg_color = settings.value("ChangedColor")
        if chg_color: self.diff_styles["chg_color"] = chg_color

        settings.endGroup()


    def colorized_diff(self):
        return self.action_colorized_diff.isChecked()


    def side_by_side(self):
        return self.action_diff_side_by_side.isChecked()


    def toggle_color_mode(self):
        self.update_styles()
        self.diff()


    def toggle_display_mode(self):
        self.diff()


    def copy_from_clipboard1(self):
        self.text_1.setPlainText(self.get_clipboard())
        self.diff()


    def copy_from_clipboard2(self):
        self.text_2.setPlainText(self.get_clipboard())
        self.diff()


    def get_clipboard(self) -> str:
        return self.clipboard.text(QClipboard.Selection)


    def text_changed(self):
        # Don't update the diff on every textChanged event
        self.progress_bar.setRange(0, 0)
        self.timer.start(200) # ms


    def on_timer_update_diff(self):
        self.diff()


    def diff(self):
        scrollbar_v = self.text_diff.verticalScrollBar()
        scrollbar_h = self.text_diff.horizontalScrollBar()
        old_vertical_pos: int = scrollbar_v.value()

        text1: str = self.text_1.toPlainText().split("\n")
        text2: str = self.text_2.toPlainText().split("\n")

        diff: str = ""
        if self.side_by_side():
            gen = self.html_diff.make_file(
                text1,
                text2,
                "left",
                "right",
                context=False,
                numlines=3
            )
            for line in gen:
                diff += line

        else:
            gen = difflib.unified_diff(
                text1,
                text2,
                "left",
                "right"
            )
            for line in gen:
                diff += self.format_inline(line) + "<br />\n"

        self.text_diff.setHtml(diff)
        scrollbar_v.setValue(old_vertical_pos)
        self.progress_bar.setRange(0, 100)


    def format_inline(self, text: str) -> str:
        colorized: bool = self.colorized_diff()

        text = html.escape(text)

        if re.search("^\+", text):
            if colorized:
                return """
                    <span style=\"background-color: %s\">%s</span>
                """ % (
                    self.diff_styles["add_color"],
                    text
                )

            else:
                return f"<strong>{text}</strong>"

        elif re.search("^\-", text):
            if colorized:
                return """
                    <span style=\"background-color: %s\">%s</span>
                """ % (
                    self.diff_styles["sub_color"],
                    text
                )

            else:
                return f"<em>{text}</em>"

        else:
            return text
